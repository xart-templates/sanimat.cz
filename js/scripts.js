jQuery.noConflict();
jQuery(document).ready(function(){

	//slideshow
	jQuery('#slideshow').nivoSlider({effect:'sliceUpLeft',animSpeed:600,pauseTime:4000,directionNav:false,captionOpacity:1});
	jQuery('.nivo-control').text('').append('<i/>');
	
	jQuery('.mod_custom-eshop-slider').append('<a class="prev"/><a class="next"/>');
	jQuery('.mod_custom-eshop-slider .scrollable').scrollable({circular:true,speed:600}).autoscroll(8000);
	
	var height = jQuery('#col-left').height();
	jQuery('#content').css({'min-height':height-20});
	
	// tabs
	jQuery('.panes .item').hide();
      jQuery('.panes .item:first-child').show();
      jQuery('.tabs li:first-child a').addClass('act');
      jQuery('.tabs li a').click(function () {
        var index = jQuery('.tabs li a').index(this);
        var index1 = index+1;
        jQuery(this).parents('.tabs').find('a').removeClass('act');
        jQuery(this).addClass('act');
        jQuery(this).parents('.tabs').next('.panes').find('.item').hide();
        jQuery(this).parents('.tabs').next('.panes').find('.item:nth-child(' + (index1++) + ')').show();
      });
	
	// pie
	jQuery('.mod_mainmenu,.mod_jflanguageselection a,.mod_search input,#home-header,#container,.mod_newsflash,.mod_custom-kontakt,.mod_custom-kontakt .phone,.mod_custom-submenu,.mod_custom-about,#content,#content h1,.formatted form.normal-form input.input-field-long,.formatted form.normal-form input.input-field-short,.formatted form.normal-form textarea,.formatted form.normal-form select,.normal-button,.pagehub span').addClass('pie');

	
	// pngfix
	jQuery('.nav-buttons li,.nav-buttons li a,.mod_custom-kontakt header i').addClass('pngfix');
	
});
